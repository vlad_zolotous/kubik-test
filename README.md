# kubik-test


build and publish example-app docker image:
```
git clone https://gitlab.com/vlad_zolotous/kubik-test.git
cd kubik-test/app
docker build -t karmashkin/kubik-example:latest .
docker push karmashkin/kubik-example:latest

```

kebernates start pod and service example-app:
```
kubectl apply -f https://gitlab.com/vlad_zolotous/kubik-test/-/raw/main/kubik-example-app.yaml

kubectl get pods --all-namespaces -o wide
kubectl get services --all-namespaces -o wide

ubectl describe pod kubik-example-pod
kubectl describe svc kubik-example-svc

curl http://{WORKER_IP}:31700/

kubectl delete pod kubik-example-pod
kubectl delete svc kubik-example-svc

```

